<?php

require('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("Shaun");

echo "Name : $sheep->name <br>";  
echo "legs : $sheep->legs <br>"; 
echo "cold blooded : $sheep->coldblooded <br>";
echo "<br>";

$kodok = new Frog("Buduk");

echo "Name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->coldblooded <br>";
echo "Jump : ";$kodok->jump();
echo "<br> <br>";

$sungokong = new Ape("Kera Sakti");

echo "Name : $sungokong->name <br>";
echo "legs : $sungokong->legs <br>";
echo "cold blooded : $sungokong->coldblooded <br>";
echo "yell : ";$sungokong->yell();
echo "<br>";


